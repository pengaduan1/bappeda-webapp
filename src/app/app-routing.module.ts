import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './auth/login/login.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/landing/dashboard',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preloadingStrategy: PreloadAllModules,
    anchorScrolling: 'enabled',
    scrollPositionRestoration: 'enabled',
    initialNavigation: 'enabled',
    useHash: true
  })],
  exports: [RouterModule]

})
export class AppRoutingModule { }
