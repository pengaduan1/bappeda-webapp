import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {UserAdmin} from './shared/models/auth/userAdmin';
import {environment} from '../environments/environment';
import {map} from 'rxjs/operators';
import {StatusLogin} from './shared/models/auth/statusLogin';

@Injectable()
export class AuthService {
  loggedIn = false;

  constructor(private router: Router, private httpKlien: HttpClient){

  }

  isLogin: boolean | undefined = false;
  // private currentLogin = 'access_token'

  login(username: string, password: string): void{
    const userAdmin = new UserAdmin();
    userAdmin.username = username;
    userAdmin.password = password;
    console.log(userAdmin);
    this.httpKlien.post(environment.baseUrl + '/login', userAdmin
    ).pipe(map(data => data as StatusLogin))
      .subscribe( data => {
        this.isLogin = data.isValid;
        if (this.isLogin){
          localStorage.setItem('isLogin', 'Y');
          // @ts-ignore
          localStorage.setItem('token', data.token);
          localStorage.setItem('username', username);
          this.router.navigate(['/admin-page/dashboard']);
        }
      });
  }

  isAuthenticated(): boolean{
    const username = localStorage.getItem('username');
    const isLogin = localStorage.getItem('isLogin');
    const token = localStorage.getItem('token');
    const userAdmin = new UserAdmin();
    userAdmin.username = username;
    userAdmin.token = token;
    let statusLogin = false;
    if (token != null && username != null && isLogin === 'Y'){
      statusLogin = true;
    }
    return statusLogin;
  }

  // @ts-ignore
  isAuthorized(allowedRoles: string[]): Observable<StatusLogin> {
    const username = localStorage.getItem('username');
    const token = localStorage.getItem('token');
    if (username != null && token != null) {
      console.log(allowedRoles);
      const userAdmin = new UserAdmin();
      userAdmin.username = username;
      userAdmin.token = token;
      return this.httpKlien.post(environment.baseUrl + '/ceklogin', userAdmin
      ).pipe(map( data => data as StatusLogin));
    } else {
      this.router.navigate(['/admin-page/login']);
    }
  }

  // tslint:disable-next-line:typedef
  logout(){
    localStorage.removeItem('token');
    localStorage.clear();
    this.router.navigate(['/admin-page/login']);
  }

}
