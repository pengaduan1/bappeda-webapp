import { NgModule } from '@angular/core';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';

import {AdminComponent} from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PengaduanComponent } from './pengaduan/pengaduan.component';
import { PengambilanDataComponent } from './pengambilan-data/pengambilan-data.component';
import {SharedModule} from '../../shared/shared.module';
import { DetailPengaduanComponent } from './pengaduan/detail-pengaduan/detail-pengaduan.component';
import {DataTablesModule} from 'angular-datatables';
import { TDanSdaComponent } from './pengambilan-data/t-dan-sda/t-dan-sda.component';
import { LhDanTrComponent } from './pengambilan-data/lh-dan-tr/lh-dan-tr.component';
import { PpDanPComponent } from './pengambilan-data/pp-dan-p/pp-dan-p.component';
import { DataSampahComponent } from './pengambilan-data/lh-dan-tr/data-sampah/data-sampah.component';
import { DataLahanKritisComponent } from './pengambilan-data/lh-dan-tr/data-lahan-kritis/data-lahan-kritis.component';
import { DataIklhComponent } from './pengambilan-data/lh-dan-tr/data-iklh/data-iklh.component';
// tslint:disable-next-line:max-line-length
import { DataDaerahRawanBencanaComponent } from './pengambilan-data/lh-dan-tr/data-daerah-rawan-bencana/data-daerah-rawan-bencana.component';
import { DataSanitasiComponent } from './pengambilan-data/pp-dan-p/data-sanitasi/data-sanitasi.component';
import { DataJalanLingkunganComponent } from './pengambilan-data/pp-dan-p/data-jalan-lingkungan/data-jalan-lingkungan.component';
import { DataAirBersihComponent } from './pengambilan-data/pp-dan-p/data-air-bersih/data-air-bersih.component';
import { DataRutilahuComponent } from './pengambilan-data/pp-dan-p/data-rutilahu/data-rutilahu.component';
import { DataKawasanKumuhComponent } from './pengambilan-data/pp-dan-p/data-kawasan-kumuh/data-kawasan-kumuh.component';
import { DataRusunComponent } from './pengambilan-data/pp-dan-p/data-rusun/data-rusun.component';
import { DataGrownTankComponent } from './pengambilan-data/pp-dan-p/data-grown-tank/data-grown-tank.component';
import { DataRthComponent } from './pengambilan-data/pp-dan-p/data-rth/data-rth.component';


@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    AdminComponent,
    DashboardComponent,
    PengaduanComponent,
    PengambilanDataComponent,
    DetailPengaduanComponent,
    TDanSdaComponent,
    LhDanTrComponent,
    PpDanPComponent,
    DataSampahComponent,
    DataLahanKritisComponent,
    DataIklhComponent,
    DataDaerahRawanBencanaComponent,
    DataSanitasiComponent,
    DataJalanLingkunganComponent,
    DataAirBersihComponent,
    DataRutilahuComponent,
    DataKawasanKumuhComponent,
    DataRusunComponent,
    DataGrownTankComponent,
    DataRthComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    DataTablesModule
  ],
  bootstrap: [AdminComponent]
})
export class AdminModule { }
