import {Component, OnDestroy, OnInit} from '@angular/core';
import {PengaduanService} from '../../../shared/services/pengaduan.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Pengaduan} from '../../../shared/models/pengaduan';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-pengaduan',
  templateUrl: './pengaduan.component.html',
  styleUrls: ['./pengaduan.component.scss'],
  providers: [PengaduanService]
})
export class PengaduanComponent implements OnDestroy, OnInit {

  dtOptions: DataTables.Settings = {};
  listPengaduan: Pengaduan[] = [];
  dtTrigger: Subject<any> = new Subject<any>();


  constructor(private pengaduanService: PengaduanService,
              private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient
  ) { }

  ngOnInit(): void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      dom: 'Bfrtip',
    };
    this.pengaduanService.listpengaduan()
      .subscribe(data => {
        console.log(data);
        this.listPengaduan = data;
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      });

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


}
