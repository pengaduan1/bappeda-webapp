import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPengaduanComponent } from './detail-pengaduan.component';

describe('DetailPengaduanComponent', () => {
  let component: DetailPengaduanComponent;
  let fixture: ComponentFixture<DetailPengaduanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailPengaduanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPengaduanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
