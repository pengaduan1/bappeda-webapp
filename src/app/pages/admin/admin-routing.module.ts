import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuardService} from '../../authGuard.service';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PengaduanComponent} from './pengaduan/pengaduan.component';
import {PengambilanDataComponent} from './pengambilan-data/pengambilan-data.component';

const routes: Routes = [
  {
    path: 'admin-page',
    children: [
      {
        path: '',
        redirectTo: '/admin-page/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        // canActivate: [AuthGuardService],
        // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
        component: DashboardComponent
      },
      {
        path: 'pengaduan',
        // canActivate: [AuthGuardService],
        // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
        component: PengaduanComponent
      },
      {
        path: 'pengambilan-data',
        // canActivate: [AuthGuardService],
        // data: {allowedRoles: ['role_user', 'role_admin', 'super_admin']},
        component: PengambilanDataComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
