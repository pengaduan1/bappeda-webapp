import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PengambilanDataComponent } from './pengambilan-data.component';

describe('PengambilanDataComponent', () => {
  let component: PengambilanDataComponent;
  let fixture: ComponentFixture<PengambilanDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PengambilanDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PengambilanDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
