import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataKawasanKumuhComponent } from './data-kawasan-kumuh.component';

describe('DataKawasanKumuhComponent', () => {
  let component: DataKawasanKumuhComponent;
  let fixture: ComponentFixture<DataKawasanKumuhComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataKawasanKumuhComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataKawasanKumuhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
