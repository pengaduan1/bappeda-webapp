import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataRthComponent } from './data-rth.component';

describe('DataRthComponent', () => {
  let component: DataRthComponent;
  let fixture: ComponentFixture<DataRthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataRthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataRthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
