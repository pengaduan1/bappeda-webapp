import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataAirBersihComponent } from './data-air-bersih.component';

describe('DataAirBersihComponent', () => {
  let component: DataAirBersihComponent;
  let fixture: ComponentFixture<DataAirBersihComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataAirBersihComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataAirBersihComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
