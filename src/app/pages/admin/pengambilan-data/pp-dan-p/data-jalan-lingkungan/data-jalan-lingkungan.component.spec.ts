import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataJalanLingkunganComponent } from './data-jalan-lingkungan.component';

describe('DataJalanLingkunganComponent', () => {
  let component: DataJalanLingkunganComponent;
  let fixture: ComponentFixture<DataJalanLingkunganComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataJalanLingkunganComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataJalanLingkunganComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
