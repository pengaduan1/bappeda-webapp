import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataRutilahuComponent } from './data-rutilahu.component';

describe('DataRutilahuComponent', () => {
  let component: DataRutilahuComponent;
  let fixture: ComponentFixture<DataRutilahuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataRutilahuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataRutilahuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
