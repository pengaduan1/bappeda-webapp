import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataSanitasiComponent } from './data-sanitasi.component';

describe('DataSanitasiComponent', () => {
  let component: DataSanitasiComponent;
  let fixture: ComponentFixture<DataSanitasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataSanitasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataSanitasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
