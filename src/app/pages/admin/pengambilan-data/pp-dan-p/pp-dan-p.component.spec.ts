import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PpDanPComponent } from './pp-dan-p.component';

describe('PpDanPComponent', () => {
  let component: PpDanPComponent;
  let fixture: ComponentFixture<PpDanPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PpDanPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PpDanPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
