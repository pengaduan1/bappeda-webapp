import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataRusunComponent } from './data-rusun.component';

describe('DataRusunComponent', () => {
  let component: DataRusunComponent;
  let fixture: ComponentFixture<DataRusunComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataRusunComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataRusunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
