import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataGrownTankComponent } from './data-grown-tank.component';

describe('DataGrownTankComponent', () => {
  let component: DataGrownTankComponent;
  let fixture: ComponentFixture<DataGrownTankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataGrownTankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataGrownTankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
