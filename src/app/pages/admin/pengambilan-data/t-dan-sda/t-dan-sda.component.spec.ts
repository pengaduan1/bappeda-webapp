import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TDanSdaComponent } from './t-dan-sda.component';

describe('TDanSdaComponent', () => {
  let component: TDanSdaComponent;
  let fixture: ComponentFixture<TDanSdaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TDanSdaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TDanSdaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
