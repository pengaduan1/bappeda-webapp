import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LhDanTrComponent } from './lh-dan-tr.component';

describe('LhDanTrComponent', () => {
  let component: LhDanTrComponent;
  let fixture: ComponentFixture<LhDanTrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LhDanTrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LhDanTrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
