import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataIklhComponent } from './data-iklh.component';

describe('DataIklhComponent', () => {
  let component: DataIklhComponent;
  let fixture: ComponentFixture<DataIklhComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataIklhComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataIklhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
