import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDaerahRawanBencanaComponent } from './data-daerah-rawan-bencana.component';

describe('DataDaerahRawanBencanaComponent', () => {
  let component: DataDaerahRawanBencanaComponent;
  let fixture: ComponentFixture<DataDaerahRawanBencanaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataDaerahRawanBencanaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDaerahRawanBencanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
