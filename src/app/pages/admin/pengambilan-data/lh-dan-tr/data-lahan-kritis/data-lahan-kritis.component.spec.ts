import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataLahanKritisComponent } from './data-lahan-kritis.component';

describe('DataLahanKritisComponent', () => {
  let component: DataLahanKritisComponent;
  let fixture: ComponentFixture<DataLahanKritisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataLahanKritisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataLahanKritisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
