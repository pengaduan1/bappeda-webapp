import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataSampahComponent } from './data-sampah.component';

describe('DataSampahComponent', () => {
  let component: DataSampahComponent;
  let fixture: ComponentFixture<DataSampahComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataSampahComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataSampahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
