import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-root',
  templateUrl: './admin.component.html'
})
export class AdminComponent {
  title = 'bappeda-webapp';
}
