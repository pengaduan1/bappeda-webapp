import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PengaduanComponent } from './pengaduan/pengaduan.component';
import {LandingComponent} from './landing.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@NgModule({
  declarations: [DashboardComponent, PengaduanComponent, LandingComponent],
    imports: [
        CommonModule,
        LandingRoutingModule,
        SharedModule,
        NgxChartsModule,
    ]
})
export class LandingModule { }
