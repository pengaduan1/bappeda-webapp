import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PengaduanComponent} from './pengaduan/pengaduan.component';

const routes: Routes = [
  {
    path: 'landing',
    children: [
      {
        path: '',
        redirectTo: '/landing/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'pengaduan',
        component: PengaduanComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
