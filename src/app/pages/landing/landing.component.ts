import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'landing-root',
  templateUrl: './landing.component.html'
})
export class LandingComponent {
  title = 'bappeda-webapp';
}
