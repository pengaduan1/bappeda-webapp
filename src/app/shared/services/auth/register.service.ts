import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class RegisterService{
  constructor(private http: HttpClient) { }

  // tslint:disable-next-line:typedef
  public saveAdmin(value: any){
    return this.http.post<any>(`${environment.baseUrl}/saveAdmin`, value, {observe : 'response'});
  }

  // tslint:disable-next-line:typedef
  public saveUser(value: any){
    return this.http.post<any>(`${environment.baseUrl}/saveUser`, value, {observe : 'response'});
  }
}
