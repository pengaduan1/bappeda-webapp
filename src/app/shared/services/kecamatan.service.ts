import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {Kecamatan} from '../models/kecamatan';

@Injectable()
export class KecamatanService{
  constructor(private httpKlien: HttpClient){

  }

  listkecamatan( ): Observable<Kecamatan[]> {
    return this.httpKlien.get(environment.baseUrl + '/listkecamatanjson')
      .pipe(map(data => data as Kecamatan[]));
  }

}
