import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../auth.service';

@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.scss'],
  providers: [AuthService]
})
export class AdminNavbarComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  onLogout(){
    this.authService.logout();
  }


}
