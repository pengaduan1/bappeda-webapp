import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminNavbarComponent } from './components/navbar/admin-navbar/admin-navbar.component';
import { LandingNavbarComponent } from './components/navbar/landing-navbar/landing-navbar.component';
import { AdminSidebarComponent } from './components/sidebar/admin-sidebar/admin-sidebar.component';
import { LandingSidebarComponent } from './components/sidebar/landing-sidebar/landing-sidebar.component';
import { AdminFooterComponent } from './components/footer/admin-footer/admin-footer.component';
import { LandingFooterComponent } from './components/footer/landing-footer/landing-footer.component';



@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [AdminNavbarComponent, LandingNavbarComponent, AdminSidebarComponent, LandingSidebarComponent, AdminFooterComponent, LandingFooterComponent],
  exports: [
    AdminNavbarComponent,
    LandingNavbarComponent,
    AdminSidebarComponent,
    LandingSidebarComponent,
    AdminFooterComponent,
    LandingFooterComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
