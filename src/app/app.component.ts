import { Component } from '@angular/core';
import {AdminModule} from './pages/admin/admin.module';
import {LandingModule} from './pages/landing/landing.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  adminModule?: AdminModule;
  landingModule?: LandingModule;

  title = 'bappeda-webapp';
}
